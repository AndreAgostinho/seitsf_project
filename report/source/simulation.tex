\section{Calculations and simulation models}\label{sec:simulation}

\subsection{PV array production}
\subsubsection{Incident irradiance}

The irradiance incident in a PV array is dependent on the sun position, weather conditions and array orientation (tilt and azimuth). To calculate PV production the irradiance incident in the plane of array (POA) must be calculated. According to \cite{pv_model_POA} POA irradiance can be calculated as the sum of three components:

\[
E_{POA} = E_b + E_g + E_d
\]

Where $E_{POA}$ [\si{\W/\m^2}] is the plane of array irradiance, $E_b$ [\si{\W/\m^2}] is the beam component, $E_g$ [\si{\W/\m^2}] is the ground reflected component and $E_d$ [\si{\W/\m^2}] is the sky-diffuse component. Each of this components can be calculated as follows:

\[
E_b = DNI \times cos(AOI)
\]

\[
E_g = GHI \times albedo \times \frac{1-cos(\theta_T)}{2}
\]

\[
E_d = DHI \times \frac{1 + cos(\theta_T)}{2}
\]

Where $GHI$~[\si{\W/\m^2}] is the global horizontal irradiance - the total irradiance received by a surface placed horizontal; 
$DNI$~[\si{\W/\m^2}] is the direct normal irradiance - irradiance arriving in a direct line from the sun as measured on a surface held perpendicular to the sun; 
$DHI$~[\si{\W/\m^2}] is the diffuse horizontal irradiance - the irradiance that is scattered by the atmosphere and received by an horizontal surface; 
$\theta_Z$~[\si{\degree}] is the sun zenith - the angle between a line perpendicular to the earth's surface and the sun; 
$\theta_A$~[\si{\degree}] is te sun azimuth - the angle between a line pointing due north to the sun's current position in the sky; 
$\theta_T$~[\si{\degree}] is the array tilt angle; 
$\theta_{A,array}$~[\si{\degree}] is the array azimuth and $AOI$~[\si{\degree}] is the angle of incidence between the Sun’s rays and the PV array which can be calculated as:

\[
AOI = cos^{-1}[cos(\theta_Z) cos(\theta_T) + sin(\theta_Z) sin(\theta_T) cos(\theta_A - \theta_{A, array})]
\]

The Albedo is the fraction of the $GHI$ that is reflected by the ground which is about $0.15$ for urban environment \cite{pv_model_albedo}.

The sky-diffuse irradiance ($E_d$) was calculated using the Isotropic Sky Diffuse Model \cite{pv_model_E_d_iso} which is the simplest model to calculate this irradiance.

To perform the previously referenced calculations solar data containing GHI, DNI, DHI, $\theta_Z$ and $\theta_A$ every hour is necessary. Fortunately Solcast solar data provides all this parameters \cite{solcast_specs}.

\subsubsection{Deratings}
Although PV panel manufacturers only supply a single efficiency value for their panels, the panel efficiency changes with a number of factors two of them being panel operating temperature and ageing.

To calculate the panel operation temperature a very simplistic model was used that assumes that the panel temperature rise above ambient temperature is directly proportional to the received irradiance and uses as reference the panel test data given by the manufacturers in the panel's datasheet. The formula used to calculate the panel temperature is the following:
\[
T_{PV} = T_{air} + \frac{E_{POA}}{G_{test}} \times (T_{PV, test}-T_{air, test})
\]
where $T_{PV}$~[\si{\celsius}] is the panel operating temperature, $T_{air}$~ [\si{\celsius}] is the ambient temperature, $G_{test}$~[\si{\W/\m^2}] is the panel test irradiance, $T_{PV, test}$~[\si{\celsius}] is the panel test working temperature and $T{air, test}$~[\si{\celsius}] is the test ambient temperature. The test data provided by the manufacturers is usually in NOCT conditions, meaning $G_{test} = \SI{800}{\W/\m^2}$ and $T_{air, test} = \SI{20}{\celsius}$, usually resulting in an operating temperature $T_{PV, test} \approx \SI{45}{\celsius}$.

The panel derating to to temperature is then calculated using the following expression:

\[
der_T = 1 + P_{tempco}/100 \times (T_{PV}-T_{PV, test})
\]

where $der_T$ is a multiplicative factor to apply the temperature derating to the panel production and $P_{tempco}$~[\si{\%/\K}] is the panel power temperature coefficient given in the panel datasheet and usually about \SI{-0.4}{\%/\K} for silicon panels.

See \autoref{sec:equipment} for the calculation of the panel ageing derating, $der_{age}$.

\subsubsection{Output}

The solar input power is calculated as:
\[
P_{in} = E_{POA} \times PV_{area}
\]

The PV power output is then calculated using the following expression:
\[
P_{PV} = P_{in} \times \eta_{PV} \times der_T \times der_{age}
\]
where $P_{PV}$ is the power produced by the panel/array, $PV_{area}$ is the total panel/array area and $\eta_{PV}$ is the panels efficiency as given by the datasheet.

The matlab code used to make all panel production calculations is listed in \autoref{lst:sim_PV}.

\subsection{Battery simulation}

Batteries have a number of limits that must be respected for correct operation of the system. This limits are specified for each battery in the equipment section (\autoref{sec:equipment}).

When integrating the batteries in the system the system needs to be simulated time step by time step because the battery behaviour changes with its SoC.
At each time step (each hour of day) the SoC of the battery is checked against its predefined minimum and maximum limits and the battery is only charged or discharge until is reaches one of those limits.
The maximum power supplied or absorbed by the battery at each time step is also limited by the maximum charge and discharge ratings of the batteries.

The function created to perform this checks and calculate the power supplied or absorbed by the battery and the SoC of the battery is listed in \autoref{lst:sim_bat}.

\subsection{System simulation}

The system simulation is very simple. The program simply imposes that at each time step:
\[
P_{load} = P_{PV} + P_{bat} + P_{grid}
\]

The program is supplied with load data and solar data in some interval and panel, battery and inverter information.
For each point in the load vector (every hour of the day in this case) the program first tries to supply the load with the PV production, if that's not enough it tries to supply the load with the battery stored energy and if that's not enough it draws the remaining power from the grid. If the PV production is higher than the load then it tries to charge the battery with the remaining power and the power that cannot be absorbed by the battery is sold to the grid.

The panel information passed to the program is the information from an equivalent panel that account for the number of panels in the array, for this the panel area is simply multiplied by the number of panels used.
The same is true for the battery information. The stored energy and maximum charge and discharge power are all multiplied by the number of batteries used.

The PV production calculated is multiplied by the inverter efficiency before being subtracted from the load power.

The simulation returns for each instant the power produced by the PV array, the power supplied or absorbed by the battery and the power bought or sold from the grid.

The code that implements this calculations is listed in \autoref{lst:sim_system}.

\subsection{Investment calculations}
The cost of an installation is estimated  using the following expression:
\[
    InstallCost = (n_P * price_P + n_B * price_B + price_I) * 1.20
\]

where \(n_P\) is the number of panels used, \(price_P\) is the price of Panel1, \(n_B\) is the number of batteries, \(price_B\) is the price of the battery considered, \(priceI\) is the price of the smallest inverter rated for the maximum total panel power and \(1.2\) accounts for other system costs.

From the data returned by the system simulations using a single year (8760 hours) of load and solar data one can calculate the income of the installation per year.

First the energy that doesn't need to be bought to the the grid due to PV production, $E_{saved}$~[\si{\W\hour}], and the energy sold to the grid, $E_{sell}$~[\si{\W\hour}] must be calculated:
\[
    E_{load} = \sum P_{load}
\]
\[
    E_{buy} = \sum {P_{grid}}^+
\]
\[
    E_{saved} = E_{load} - E_{buy}
\]
\[
    E_{sell} = -\sum {P_{grid}}^-
\]
Note that the expressions are only valid because when using 1 hour time steps energy in \si{\W\hour} is equal to the power in \si{\W}, else the expressions must be corrected.

The  income of the installation per year is then calculated as:
\[
    income_{year} = E_{saved} * price_{E_{buy}} + E_{sold} * price_{E_{sell}}
\]
The energy price when buying, \(price_{E_{buy}}\), is considered to be 
0.08\euro and the price when selling, \(price_{E_{sell}}\), is 0.04\euro.

The return on investment, ROI, in 8 years is then calculated as:
\[
ROI = -cost + income_{year} * 8
\]
and the years to break even:
\[
    BreakEven = InstallCost / income_{year}
\]