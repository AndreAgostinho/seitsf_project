%% sim_PV.m
function [pv_P,pv_T, pv_G, pv_T_der] = sim_PV(panel, solar_data)
% sim_PV calculate PV output using solar data and panel characteristics

    % input power
    pv_G = calc_pv_G(panel, solar_data);
    P_in = pv_G * panel.area;
    
    % temperature derating
    pv_T = solar_data.AirTemp + (pv_G/panel.test_G)* (panel.work_T-panel.test_T);
    pv_T_der = 1 + (panel.tempco_P/100) * (pv_T-panel.test_T);
    
    % other deratings
    other_derating = panel.derating;
    
    % final PV power
    pv_P = P_in * panel.eff/100 .* pv_T_der * other_derating;
end

function [pv_G] = calc_pv_G(panel, solar_data)
% calc_pv_G calculate irradiance in a panel
%   considerss tilt and azimuth of panel, using Ghi, Dni, Dhi and sun
%   azimuth and zenith
    
    az = solar_data.Azimuth /180 * pi;
    ze = solar_data.Zenith / 180 * pi;
    albedo = 0.15; % assume albedo for urban environment
    tilt = panel.tilt / 180 * pi;
    az_array = panel.az / 180 * pi;
    
    % calc source https://pvpmc.sandia.gov/modeling-steps/1-weather-design-inputs/plane-of-array-poa-irradiance/
    % angle of incidence (AOI) between the panels and the sun rays
    AOI = acos(cos(ze).*cos(tilt) + sin(ze).*sin(tilt).*cos(az-az_array));
    % direct beam irradiance
    E_b = max(solar_data.Dni .* cos(AOI), 0);
    % ground reflected irradiance
    E_g = solar_data.Ghi * albedo .* (1-cos(tilt))/2;
    % sky diffuse irradiane
    E_d = solar_data.Dhi .* (1+cos(tilt))/2;

    % total irradiance
    pv_G = E_b + E_g + E_d;
end