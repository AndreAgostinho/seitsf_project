%% sim_bat.m
function [new_SoC, bat_P] = sim_bat(bat, SoC, power)
% given a battery, it's current SoC and the power that is available for the
% battery to supply or absorve, calculates how much power can really be put
% or taken from the battery and the new battery SoC
    if (bat.E_Wh == 0)
        new_SoC = SoC;
        bat_P = 0;
        return;
    end
    
    if (power >= 0) % discharging
        disch_E = bat.E_Wh * (SoC - bat.min_SoC);
        bat_P = min([power, disch_E, bat.max_P]);
    elseif (power < 0) % charging
        charge_E = bat.E_Wh * (bat.max_SoC - SoC);
        bat_P = -min([-power, charge_E, bat.max_Pch]);
    end
    new_SoC = (SoC * bat.E_Wh - bat_P) / bat.E_Wh;

end

