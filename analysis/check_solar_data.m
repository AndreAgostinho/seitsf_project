addpath('../data');

solar_data = readtable('solar_data_station_481.csv');

figure
plot(solar_data.PeriodStart, solar_data.AirTemp)
figure
hold on
plot(solar_data.PeriodStart, solar_data.Ghi)
plot(solar_data.PeriodStart, solar_data.Dni)
plot(solar_data.PeriodStart, solar_data.Dhi)
plot(solar_data.PeriodStart, solar_data.Ebh)
