%% sim_system_v2.m
function [P_pv,P_bat, P_grid, bat_SoC, pv_T, pv_G, pv_T_der]...
    = sim_system_v2(panel, bat, inv, P_load, solar_data)
%SIM_SYSTEM Simulate the sistem's power transit
%   Assumes the input PV and battery is the equivalent PV and battery if
%   multiple of each are used

bat_SoC = zeros(length(P_load) + 1, 1);
bat_SoC(1) = bat.init_SoC;
P_bat = zeros(length(P_load), 1);
P_grid = zeros(length(P_load), 1);

% calc panel production and multiply by number of panels
[P_pv, pv_T, pv_G, pv_T_der] = sim_PV(panel, solar_data);

% simulate PV, battery, grid dynamics
% power_load = power_panel + power_battery + power_grid
for i = 1:length(P_load)
    balance = P_load(i);
    % discount the PV production from the balance
    balance = balance - inv.eff*P_pv(i);
    % try to put or take the remaining power from battery
    [bat_SoC(i+1), P_bat(i)] = sim_bat(bat, bat_SoC(i), balance);
    balance = balance - P_bat(i);
    % put of take the remaining power from the grid
    P_grid(i) = balance;
    
    balance = balance - P_grid(i);
    %at this point power_balance should always be 0
end

end

