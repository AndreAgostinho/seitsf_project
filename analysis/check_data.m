addpath('../data');

station_readings = readtable('station_readings.csv');
energy_costs = readtable('energy_cost_periods.csv');
station_arrivals = readtable('station_arrivals.csv');

station = 484;
select = (station_readings.station_id == station);
readings = station_readings.reading(select);
energy_used = readings(4:6) - readings(1:3);
total_energy_used = sum(energy_used);
%total_energy_cost = total_energy_used * energy_costs.cost_per_kwh(4)

% time = datetime(station_arrivals.arrival_date,'InputFormat','dd/MM/yyy');
% time.Format = 'dd/MM/yyy HH:mm';
% time.Hour = station_arrivals.arrival_time;
% figure
% histogram(station_arrivals.datetime,'BinMethod','hour');
select = (station_arrivals.arrival_station == station);
% figure
histogram(station_arrivals.arrival_time(select),'BinMethod','month');
figure
[counts,bins] = histcounts(station_arrivals.arrival_time(select),'BinMethod','hour');
plot(bins(2:end),counts)