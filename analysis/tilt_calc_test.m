addpath('../data');

solar_data = readtable('solar_data_station_481.csv');
az = solar_data.Azimuth /180 * pi;
ze = solar_data.Zenith / 180 * pi;
tilt = 0;
az_array = pi;
albedo = 0.2;

AOI = acos(cos(ze).*cos(tilt) + sin(ze).*sin(tilt).*cos(az-az_array));
E_b = max(solar_data.Dni .* cos(AOI), 0);
E_g = solar_data.Ghi * albedo .* (1-cos(tilt))/2;
E_d = solar_data.Dhi .* (1+cos(tilt))/2;

E_POA = E_b + E_g + E_d;

figure
plot(E_POA)
hold on
plot(solar_data.Ghi);
