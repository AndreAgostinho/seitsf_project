close all
addpath('../data');
processed_data_path = '../data/processed/';

%% import data
station_readings = readtable('station_readings.csv');
energy_costs = readtable('energy_cost_periods.csv');
station_arrivals = readtable('station_arrivals.csv');

%% select the station
station = 484;

%% calculate the energy used per trip
% select relevant data
sel_station = (station_readings.station_id == station);
readings = station_readings.reading(sel_station);
energy_used = readings(4:6) - readings(1:3);
total_energy_used = sum(energy_used);

% calc some energy values
energy_per_day = total_energy_used/...
    days(station_readings.reading_t(7)-station_readings.reading_t(1));
money_per_day = energy_per_day * energy_costs.cost_per_kwh(4);
money_per_month = money_per_day * 30;


sel_station = station_arrivals.arrival_station == station;
sel_E_byke = string(station_arrivals.bike_type) == 'E';
sel_meas_time = station_arrivals.arrival_time < station_readings.reading_t(7)...
    & station_arrivals.arrival_time > station_readings.reading_t(1);

select = (sel_station & sel_E_byke & sel_meas_time);

[counts,bins] = histcounts(station_arrivals.arrival_time(select),'BinMethod','hour');

n_rides = sum(counts);
energy_per_ride = total_energy_used/n_rides;

% draw the power profile for the measurement time
figure
hold on
stairs(bins(1:end-1),counts)
yyaxis right
stairs(bins(1:end-1),counts*energy_per_ride)%power

%% calculate average daily, weekly, monthly and yearly power profiles
% selact relevant data
year_start = datetime('21/11/2018','InputFormat', 'dd/MM/yyyy');
year_finish = datetime('21/11/2019','InputFormat', 'dd/MM/yyyy');
sel_one_year = station_arrivals.arrival_time >= year_start ...
    & station_arrivals.arrival_time < year_finish;
select = sel_station & sel_one_year & sel_E_byke;
arrivals = station_arrivals(select,:);

% count weekdays
[weekday_count,~] = histcounts(day(year_start:year_finish-days(1), 'dayofweek'));

% hourly profile in a day
GS_day = groupsummary(arrivals, 'arrival_time','hourofday','IncludeEmptyGroups',true);

av_power_profile_day = GS_day.GroupCount * energy_per_ride / days(year_finish-year_start);
figure
% stair plot (add 24th hour to plot corretly the last hour)
stairs([GS_day.hourofday_arrival_time; '24'], [av_power_profile_day; av_power_profile_day(end)]);
title('Hourly power profile in a day')
xlabel('Hour of day')
ylabel('Power [kW]')
writematrix(av_power_profile_day, [processed_data_path 'power_profile_day_' num2str(station) '.csv']);

% daily profile in a week
GS_week = groupsummary(arrivals, 'arrival_time','dayname');

av_energy_profile_week = GS_week.GroupCount * energy_per_ride ./ weekday_count';
figure
bar(GS_week.dayname_arrival_time, av_energy_profile_week);
title('Daily energy consumption in a week')
xlabel('Day of week')
ylabel('Energy [kWh]')
writematrix(av_energy_profile_week, [processed_data_path 'energy_profile_week_' num2str(station) '.csv']);

% daily profile in a month
GS_month = groupsummary(arrivals, 'arrival_time','dayofmonth');

av_energy_profile_month = GS_month.GroupCount * energy_per_ride / 12;
figure
bar(GS_month.dayofmonth_arrival_time, av_energy_profile_month);
title('Daily energy consumption in a month')
xlabel('Day of month')
ylabel('Energy [kWh]')


% monthly profile in a year
GS_year = groupsummary(arrivals, 'arrival_time','monthname');

av_energy_profile_year = GS_year.GroupCount * energy_per_ride;
figure
bar(GS_year.monthname_arrival_time, av_energy_profile_year);
title('Montlhy energy consumption in a year')
xlabel('Month')
ylabel('Energy [kWh]')
writematrix(av_energy_profile_year, [processed_data_path 'energy_profile_year_' num2str(station) '.csv']);

%% calculate average values
av_energy_per_day = height(arrivals) * energy_per_ride / 365;
cost_per_day = av_energy_per_day * energy_costs.cost_per_kwh(4);
av_energy_per_week = height(arrivals) * energy_per_ride / (365/7);
cost_per_week = av_energy_per_week * energy_costs.cost_per_kwh(4);
av_energy_per_month = height(arrivals) * energy_per_ride / (365/30);
cost_per_month = av_energy_per_month * energy_costs.cost_per_kwh(4);
av_energy_per_year = height(arrivals) * energy_per_ride;
cost_per_year = av_energy_per_year * energy_costs.cost_per_kwh(4);
cost_per_5_year = 5 * cost_per_year;

%% calculate some edge cases (max/min consumptions)
% maximum power peak
[power_profile_hour, hour_bins] = histcounts(arrivals.arrival_time, 'BinMethod','hour');
power_profile_hour = power_profile_hour * energy_per_ride;
writematrix(power_profile_hour, [processed_data_path 'power_profile_hour_' num2str(station) '.csv']);
[max_power_hour, max_power_index] = max(power_profile_hour);
max_power_date = hour_bins(max_power_index); % peak date

% maximum energy in a day
[energy_profile_day, day_bins] = histcounts(arrivals.arrival_time, 'BinMethod','day');
[max_energy_day, max_energy_day_index] = max(energy_profile_day * energy_per_ride);
max_energy_day_date = day_bins(max_energy_day_index); % peak day

% minimum energy in a day
[min_energy_day, min_energy_day_index] = min(energy_profile_day * energy_per_ride);
min_energy_day_date = day_bins(min_energy_day_index); % peak day

% plot power profile days
select_max_day = arrivals.arrival_time >= max_energy_day_date ...
    & arrivals.arrival_time < max_energy_day_date + days(1);
select_min_day = arrivals.arrival_time >= min_energy_day_date ...
    & arrivals.arrival_time < min_energy_day_date + days(1);
GS_max = groupsummary(arrivals(select_max_day,:), 'arrival_time','hourofday','IncludeEmptyGroups',true);
GS_min = groupsummary(arrivals(select_min_day,:), 'arrival_time','hourofday','IncludeEmptyGroups',true);
GS_max.GroupCount = GS_max.GroupCount * energy_per_ride;
GS_min.GroupCount = GS_min.GroupCount * energy_per_ride;
figure
hold on
stairs([GS_day.hourofday_arrival_time; '24'], [av_power_profile_day; av_power_profile_day(end)]);
stairs([GS_day.hourofday_arrival_time; '24'], [GS_max.GroupCount; GS_max.GroupCount(end)]);
stairs([GS_day.hourofday_arrival_time; '24'], [GS_min.GroupCount; GS_min.GroupCount(end)]);
title('Hourly power profile in a day')
xlabel('Hour of day')
ylabel('Power [kW]')
legend({'Average profile','Maximum day','Minimum day'})

% maximum energy in a week
[energy_profile_week, week_bins] = histcounts(arrivals.arrival_time, 'BinMethod','week');
[max_energy_week, max_energy_week_index] = max(energy_profile_week * energy_per_ride);
max_energy_week_date = week_bins(max_energy_week_index); % peak week

% minimum energy in a week
[min_energy_week, min_energy_week_index] = min(energy_profile_week * energy_per_ride);
min_energy_week_date = week_bins(min_energy_week_index); % peak week

