close all
addpath('../data/');
addpath('../data/processed/')
%% import data

station_id = 484;
solar_data = readtable('solar_data_station_481.csv');
P_load = readmatrix(['power_profile_hour_' num2str(station_id) '.csv']);
P_load = P_load' * 1000; %convert kW to W
time = solar_data.PeriodStart;

% get equipment characteristics
panels = table2struct(readtable('panel_info.csv'));
batteries = table2struct(readtable('battery_info.csv'));
inverters = table2struct(readtable('inverter_info.csv'));
% get station info
stations = readtable('station_info.csv');
%% load stats

E_load_total = sum(P_load)
%% Simulate panel production for different tilts and azimuth w temperature influence

panel_id = 1;
panel = panels(panel_id);
% 35º should be optimum tilt in Portugal
tilt = [0, 10, 20, 25, 30, 35, 40];
% 180 is south (the optimum),
% -110 and 70 are perpendicular to station 481
% 90 and -90 are perpendicular to station 484
az = [180, -110, 70, 90, -90];
% no battery
bat = batteries(1);
bat.init_SoC = 0.6;
bat.E_Wh = 0;

for a = 1:length(az)
    for t = 1:length(tilt)
        panel.tilt = tilt(t);%º
        panel.az = az(a);
        
        %perform simulation
        [P_pv,P_bat, P_grid, bat_SoC, pv_T, pv_G] = sim_system(panel, bat, P_load, solar_data);
    
        % [report] energy produced by one horizontal panel in one year for each
        % panel
        E_1_panel_tilt_az(t,a) = sum(P_pv);
    end
end
% gain from different tilts and azimuths
gain = E_1_panel_tilt_az/E_1_panel_tilt_az(1,1)
writematrix(round(gain,3), '../data/processed/orientation_gain_table.csv');
%% Check if batteries are a good investment (spolier alert: they are not!)

id_panel = 1;
sel_station = stations.station_id == station_id;
max_n_panel = floor(stations(sel_station,:).length *...
    stations(sel_station,:).width / panels(panel_id).area);
n_panel = 1:max_n_panel;
n_bat = 0:3;
% assumed energy prices
energy_price_buy = 0.08;
energy_price_sell = 0.04;
roi_years = 8;

ROI = zeros(length(n_panel), length(n_bat), length(batteries));
ROI_years = zeros(length(n_panel), length(n_bat), length(batteries));

for id_bat = 1:length(batteries)
    for np = 1:length(n_panel)
        for nb = 1:length(n_bat)
            %prepare data for simulation
            panel = panels(id_panel);
            panel.area = panel.area * n_panel(np); % use x panels
            panel.tilt = 0;%º
            panel.az = 180;%º
            bat = batteries(id_bat);
            bat.init_SoC = 0.6;
            bat.E_Wh = bat.E_Wh * n_bat(nb); % use x batteries
            bat.max_P = bat.max_P * n_bat(nb);
            bat.max_Pch = bat.max_Pch * n_bat(nb);
            
            % perform simulation
            [P_pv,P_bat, P_grid, bat_SoC, pv_T, pv_G] = sim_system(panel, bat, P_load, solar_data);
            
            %calculate cost function
            energy_load_total = sum(P_load);
            energy_grid_total_pos = sum(P_grid(P_grid > 0));
    
            energy_produced = sum(P_pv);
            energy_sold = -sum(P_grid(P_grid < 0));
            energy_saved = energy_load_total - energy_grid_total_pos;
    
            equipment_cost = n_panel(np) * panel.price + n_bat(nb) * bat.price;
            if (n_panel(np)*panel.max_P < inverters(1).max_P)
                equipment_cost = equipment_cost + inverters(1).price;
            elseif (n_panel(np)*panel.max_P < inverters(2).max_P)
                equipment_cost = equipment_cost + inverters(2).price;
            else
                equipment_cost = equipment_cost + inverters(3).price;
            end
            % margin for some other minor costs
            equipment_cost = equipment_cost * 1.20;
            energy_money_savings = energy_saved/1000 * energy_price_buy...
                + energy_sold/1000 * energy_price_sell;
    
            ROI(np, nb, id_bat)  = round(-equipment_cost + energy_money_savings * roi_years, 0);
            ROI_years(np, nb, id_bat) = round(equipment_cost/energy_money_savings,1);
        end
    end
end
%% 
% Lithium battery

ROI(:,:,1)
writematrix(ROI(:,:,1), ['../data/processed/ROI_bat_LFP_' num2str(station_id) '.csv']);
ROI_years(:,:,1)
writematrix(ROI_years(:,:,1), ['../data/processed/ROI_years_bat_LFP_' num2str(station_id) '.csv']);
%% 
% AGM battery

ROI(:,:,2)
writematrix(ROI(:,:,2), ['../data/processed/ROI_bat_AGM_' num2str(station_id) '.csv']);
ROI_years(:,:,2)
writematrix(ROI_years(:,:,2), ['../data/processed/ROI_years_bat_AGM_' num2str(station_id) '.csv']);
%% simulate the production with a single horizontal panel in a year for different panels

% no battery
bat = batteries(1);
bat.init_SoC = 0.6;
bat.E_Wh = 0;

for p = 1:length(panels)
    panel = panels(p);
    panel.tilt = 0;%º
    panel.az = 180;%doesn't matter for horizontal panel
    
    %perform simulation
    [P_pv,P_bat, P_grid, bat_SoC, pv_T, pv_G, pv_T_der] = sim_system(panel, bat, P_load, solar_data);

    % [report] energy produced by one horizontal panel in one year for each
    % panel
    E_1_panel_no_tilt(p) = sum(P_pv);
    panels_0_net_E(p) = ceil(E_load_total/E_1_panel_no_tilt(p));
    Wp_0_net_E(p) = panels_0_net_E(p) * panel.max_P;
    price_0_net_E(p) = ceil(panels_0_net_E(p)) * panel.price;
end
E_1_panel_no_tilt
panels_0_net_E
Wp_0_net_E
price_0_net_E
% % [report] Panel production in a year (one of the panels)
% % irradiance received and temperature
% figure
% hold on
% plot(time, P_pv)
% ylabel('P [W]')
% title('Panel production in a year')
% % temperature
% figure
% plot(time, pv_T)
% ylabel('T [ºC]')
% title('Panel temperature in a year')
% % panel derating due to temperature
% figure
% plot(time,  pv_T_der * 100 - 100)
% ylabel('production derating [%]')
% title('Panel derating due to temperature in a year')
% % irradiance
% figure
% plot(time, pv_G)
% title('Irradiance received by a single horizontal panel')
% ylabel('G [W/m^2]')
%% Check which is the best panel

sel_station = stations.station_id == station_id;

% assumed energy prices
energy_price_buy = 0.08;
energy_price_sell = 0.04;
roi_years = 15;
tilt_panel = [0 30];

ROI = zeros(10, length(panels), length(energy_price_sell));
ROI_years = zeros(10, length(panels), length(energy_price_sell));
for ti = 1:length(tilt_panel)
    for id_panel = 1:length(panels)
        max_n_panel = floor(stations(sel_station,:).length *...
            stations(sel_station,:).width / panels(id_panel).area);
        n_panel = 1:max_n_panel;
        for np = 1:length(n_panel)
            %prepare data for simulation
            panel = panels(id_panel);
            panel.area = panel.area * n_panel(np); % use x panels
            panel.tilt = tilt_panel(ti);%º
            panel.az = 180;%º
            bat = batteries(1);
            bat.init_SoC = 0.6;
            bat.E_Wh = bat.E_Wh * 0; % use 0 batteries
            
            % perform simulation
            [P_pv,P_bat, P_grid, bat_SoC, pv_T, pv_G] = sim_system(panel, bat, P_load, solar_data);
            
            %calculate cost function
            energy_load_total = sum(P_load);
            energy_grid_total_pos = sum(P_grid(P_grid > 0));
    
            energy_produced = sum(P_pv);
            energy_sold = -sum(P_grid(P_grid < 0));
            energy_saved = energy_load_total - energy_grid_total_pos;
    
            equipment_cost = n_panel(np) * panel.price;
            if (n_panel(np)*panel.max_P <= inverters(1).max_P)
                equipment_cost = equipment_cost + inverters(1).price;
            elseif (n_panel(np)*panel.max_P <= inverters(2).max_P)
                equipment_cost = equipment_cost + inverters(2).price;
            else
                equipment_cost = equipment_cost + inverters(3).price;
            end
            % margin for some other minor costs
            equipment_cost = equipment_cost * 1.20;
            energy_money_savings = energy_saved/1000 * energy_price_buy...
                + energy_sold/1000 * energy_price_sell;
    
            ROI(np, id_panel, ti)  = round(-equipment_cost + energy_money_savings * roi_years,0);
            ROI_years(np, id_panel, ti) = round(equipment_cost/energy_money_savings,1);
        end
    end
end
%% 
% Horizontal

ROI(:,:,1)
writematrix(ROI(:,:,1), ['../data/processed/ROI_panels_only' num2str(station_id) '.csv']);
ROI_years(:,:,1)
%% 
% Optimal inclination

%ROI(:,:,2)
%ROI_years(:,:,2)